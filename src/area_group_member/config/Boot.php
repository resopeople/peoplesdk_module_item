<?php

use people_sdk\module_item\area_group_member\boot\AreaGrpMemberBootstrap;



return array(
    'people_item_area_group_member_bootstrap' => [
        'call' => [
            'class_path_pattern' => AreaGrpMemberBootstrap::class . ':boot'
        ]
    ]
);