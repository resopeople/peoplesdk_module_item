<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\group\member\model\AreaGrpMemberEntityCollection;
use people_sdk\item\area\group\member\model\AreaGrpMemberEntityFactory;
use people_sdk\item\area\group\member\model\repository\AreaGrpMemberEntityMultiRepository;
use people_sdk\item\area\group\member\model\repository\AreaGrpMemberEntityMultiCollectionRepository;



return array(
    // Area group member entity services
    // ******************************************************************************

    'people_item_area_grp_member_entity_collection' => [
        'source' => AreaGrpMemberEntityCollection::class
    ],

    'people_item_area_grp_member_entity_factory_collection' => [
        'source' => AreaGrpMemberEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_grp_member_entity_factory' => [
        'source' => AreaGrpMemberEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_area_grp_member_entity_factory_collection'],
            ['type' => 'class', 'value' => GroupEntityFactory::class],
            ['type' => 'class', 'value' => AreaEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/area_group_member/factory/group_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/area_group_member/factory/area_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_grp_member_entity_multi_repository' => [
        'source' => AreaGrpMemberEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_grp_member_entity_multi_collection_repository' => [
        'source' => AreaGrpMemberEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_area_grp_member_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_area_grp_member_entity_multi_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Preferences
    // ******************************************************************************

    AreaGrpMemberEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_area_grp_member_entity_collection']
    ]
);