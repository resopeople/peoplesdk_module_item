<?php

use people_sdk\item\area\group\member\model\AreaGrpMemberEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'area_group_member' => [
                // Area group member entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see AreaGrpMemberEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Group entity factory execution configuration array format:
                     * @see GrpMemberEntityFactory::setTabGroupEntityFactoryExecConfig() configuration format.
                     */
                    'group_factory_execution_config' => [],

                    /**
                     * Area entity factory execution configuration array format:
                     * @see AreaGrpMemberEntityFactory::setTabAreaEntityFactoryExecConfig() configuration format.
                     */
                    'area_factory_execution_config' => []
                ]
            ]
        ]
    ]
);