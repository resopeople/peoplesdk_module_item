<?php
/**
 * Description :
 * This class allows to define area group member module bootstrap class.
 * Area group member module bootstrap allows to boot area group member module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\area_group_member\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\item\area\group\member\model\AreaGrpMemberEntityFactory;
use people_sdk\module_item\area_group_member\library\ConstAreaGrpMember;



class AreaGrpMemberBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Area group member entity factory instance.
     * @var AreaGrpMemberEntityFactory
     */
    protected $objAreaGrpMemberEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param AreaGrpMemberEntityFactory $objAreaGrpMemberEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        AreaGrpMemberEntityFactory $objAreaGrpMemberEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objAreaGrpMemberEntityFactory = $objAreaGrpMemberEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstAreaGrpMember::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set area group member entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'item', 'area_group_member', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objAreaGrpMemberEntityFactory->getTabConfig(), $tabConfig);
            $this->objAreaGrpMemberEntityFactory->setTabConfig($tabConfig);
        };
    }



}