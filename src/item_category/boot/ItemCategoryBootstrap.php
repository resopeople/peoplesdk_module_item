<?php
/**
 * Description :
 * This class allows to define item category module bootstrap class.
 * Item category module bootstrap allows to boot item category module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\item_category\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\item\item\category\model\ItemCategoryEntityFactory;
use people_sdk\module_item\item_category\library\ConstItemCategory;



class ItemCategoryBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Item category entity factory instance.
     * @var ItemCategoryEntityFactory
     */
    protected $objItemCategoryEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param ItemCategoryEntityFactory $objItemCategoryEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        ItemCategoryEntityFactory $objItemCategoryEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objItemCategoryEntityFactory = $objItemCategoryEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstItemCategory::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set item category entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'item', 'item_category', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objItemCategoryEntityFactory->getTabConfig(), $tabConfig);
            $this->objItemCategoryEntityFactory->setTabConfig($tabConfig);
        };
    }



}