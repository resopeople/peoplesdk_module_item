<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\category\model\CategoryEntityFactory;
use people_sdk\item\item\model\ItemEntityFactory;
use people_sdk\item\item\category\model\ItemCategoryEntityCollection;
use people_sdk\item\item\category\model\ItemCategoryEntityFactory;
use people_sdk\item\item\category\model\repository\ItemCategoryEntityMultiRepository;
use people_sdk\item\item\category\model\repository\ItemCategoryEntityMultiCollectionRepository;



return array(
    // Item category entity services
    // ******************************************************************************

    'people_item_item_category_entity_collection' => [
        'source' => ItemCategoryEntityCollection::class
    ],

    'people_item_item_category_entity_factory_collection' => [
        'source' => ItemCategoryEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_item_category_entity_factory' => [
        'source' => ItemCategoryEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_item_category_entity_factory_collection'],
            ['type' => 'class', 'value' => CategoryEntityFactory::class],
            ['type' => 'class', 'value' => ItemEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/item_category/factory/category_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/item_category/factory/item_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_item_category_entity_multi_repository' => [
        'source' => ItemCategoryEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_item_category_entity_multi_collection_repository' => [
        'source' => ItemCategoryEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_item_category_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_item_category_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    ItemCategoryEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_item_category_entity_collection']
    ]
);