<?php

use people_sdk\item\item\category\model\ItemCategoryEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'item_category' => [
                // Item category entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see ItemCategoryEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Category entity factory execution configuration array format:
                     * @see ItemCategoryEntityFactory::setTabCategoryEntityFactoryExecConfig() configuration format.
                     */
                    'category_factory_execution_config' => [],

                    /**
                     * Item entity factory execution configuration array format:
                     * @see ItemCategoryEntityFactory::setTabItemEntityFactoryExecConfig() configuration format.
                     */
                    'item_factory_execution_config' => []
                ]
            ]
        ]
    ]
);