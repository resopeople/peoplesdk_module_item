<?php

use people_sdk\module_item\item_category\boot\ItemCategoryBootstrap;



return array(
    'people_item_item_category_bootstrap' => [
        'call' => [
            'class_path_pattern' => ItemCategoryBootstrap::class . ':boot'
        ]
    ]
);