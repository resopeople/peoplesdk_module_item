<?php

use people_sdk\item\area\member\model\AreaMemberEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'area_member' => [
                // Area member entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see AreaMemberEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * User profile entity factory execution configuration array format:
                     * @see AreaMemberEntityFactory::setTabUserProfileEntityFactoryExecConfig() configuration format.
                     */
                    'user_profile_factory_execution_config' => [],

                    /**
                     * Area entity factory execution configuration array format:
                     * @see AreaMemberEntityFactory::setTabAreaEntityFactoryExecConfig() configuration format.
                     */
                    'area_factory_execution_config' => []
                ]
            ]
        ]
    ]
);