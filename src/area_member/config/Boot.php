<?php

use people_sdk\module_item\area_member\boot\AreaMemberBootstrap;



return array(
    'people_item_area_member_bootstrap' => [
        'call' => [
            'class_path_pattern' => AreaMemberBootstrap::class . ':boot'
        ]
    ]
);