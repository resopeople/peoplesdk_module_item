<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\member\model\AreaMemberEntityCollection;
use people_sdk\item\area\member\model\AreaMemberEntityFactory;
use people_sdk\item\area\member\model\repository\AreaMemberEntityMultiRepository;
use people_sdk\item\area\member\model\repository\AreaMemberEntityMultiCollectionRepository;



return array(
    // Area member entity services
    // ******************************************************************************

    'people_item_area_member_entity_collection' => [
        'source' => AreaMemberEntityCollection::class
    ],

    'people_item_area_member_entity_factory_collection' => [
        'source' => AreaMemberEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_member_entity_factory' => [
        'source' => AreaMemberEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_area_member_entity_factory_collection'],
            ['type' => 'class', 'value' => UserProfileEntityFactory::class],
            ['type' => 'class', 'value' => AreaEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/area_member/factory/user_profile_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/area_member/factory/area_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_member_entity_multi_repository' => [
        'source' => AreaMemberEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_member_entity_multi_collection_repository' => [
        'source' => AreaMemberEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_area_member_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_area_member_entity_multi_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Preferences
    // ******************************************************************************

    AreaMemberEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_area_member_entity_collection']
    ]
);