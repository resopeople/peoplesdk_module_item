<?php

use people_sdk\item\area\model\AreaEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'area' => [
                // Area entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see AreaEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Schema entity factory execution configuration array format:
                     * @see AreaEntityFactory::setTabSchemaEntityFactoryExecConfig() configuration format.
                     */
                    'schema_factory_execution_config' => []
                ]
            ]
        ]
    ]
);