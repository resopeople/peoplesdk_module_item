<?php

use people_sdk\module_item\area\boot\AreaBootstrap;



return array(
    'people_item_area_bootstrap' => [
        'call' => [
            'class_path_pattern' => AreaBootstrap::class . ':boot'
        ]
    ]
);