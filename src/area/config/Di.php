<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\area\model\AreaEntityCollection;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\model\repository\AreaEntityMultiRepository;
use people_sdk\item\area\model\repository\AreaEntityMultiCollectionRepository;



return array(
    // Area entity services
    // ******************************************************************************

    'people_item_area_entity_collection' => [
        'source' => AreaEntityCollection::class
    ],

    'people_item_area_entity_factory_collection' => [
        'source' => AreaEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_entity_factory' => [
        'source' => AreaEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_area_entity_factory_collection'],
            ['type' => 'class', 'value' => SchemaEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/area/factory/schema_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_entity_multi_repository' => [
        'source' => AreaEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_area_entity_multi_collection_repository' => [
        'source' => AreaEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_area_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_area_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    AreaEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_area_entity_collection']
    ]
);