<?php
/**
 * Description :
 * This class allows to define schema attribute provider helper class.
 * Schema attribute provider helper allows to provide features,
 * for schema attribute provider.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\schema_attribute\helper;

use liberty_code\library\bean\model\DefaultBean;

use people_sdk\item\schema\attribute\library\ToolBoxSchemaAttrRepository;
use people_sdk\item\schema\attribute\model\repository\SchemaAttrEntitySimpleCollectionRepository;
use people_sdk\item\schema\attribute\provider\model\SchemaAttrProvider;



class SchemaAttrProviderHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Schema attribute provider instance.
     * @var SchemaAttrProvider
     */
    protected $objSchemaAttrProvider;



    /**
     * DI: Schema attribute entity collection repository instance.
     * @var SchemaAttrEntitySimpleCollectionRepository
     */
    protected $objSchemaAttrEntityCollectionRepo;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SchemaAttrProvider $objSchemaAttrProvider
     * @param SchemaAttrEntitySimpleCollectionRepository $objSchemaAttrEntityCollectionRepo
     */
    public function __construct(
        SchemaAttrProvider $objSchemaAttrProvider,
        SchemaAttrEntitySimpleCollectionRepository $objSchemaAttrEntityCollectionRepo
    )
    {
        // Init properties
        $this->objSchemaAttrProvider = $objSchemaAttrProvider;
        $this->objSchemaAttrEntityCollectionRepo = $objSchemaAttrEntityCollectionRepo;

        // Call parent constructor
        parent::__construct();
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Reset schema attribute provider.
     */
    public function reset()
    {
        // Clear cache, if required
        if(!is_null($objCacheRepo = $this->objSchemaAttrProvider->getObjCacheRepo()))
        {
            $objCacheRepo->removeTabItem($objCacheRepo->getTabSearchKey());
        };

        // Clear collection
        $objSchemaAttrEntityCollection = $this->objSchemaAttrProvider->getObjAttributeCollection();
        $objSchemaAttrEntityCollection->removeItemAll();
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load schema attributes, to get schema,
     * available for schema attribute provider.
     * Return true if success, false if an error occurs.
     *
     * Schema attribute entity collection repository execution configuration array format:
     * @see ToolBoxSchemaAttrRepository::loadSchema() configuration array format.
     *
     * @param integer $intSchemaId
     * @param null|array $tabSchemaAttrEntityCollectionRepoExecConfig = null
     * @return boolean
     */
    public function loadSchema(
        $intSchemaId,
        array $tabSchemaAttrEntityCollectionRepoExecConfig = null
    )
    {
        // Reset provider
        $this->reset();

        // Return result
        return ToolBoxSchemaAttrRepository::loadSchema(
            $this->objSchemaAttrProvider->getObjAttributeCollection(),
            $this->objSchemaAttrEntityCollectionRepo,
            $intSchemaId,
            $tabSchemaAttrEntityCollectionRepoExecConfig
        );
    }



    /**
     * Load schema attributes, to get specified profile schemas,
     * available for schema attribute provider.
     * Return true if success, false if an error occurs.
     *
     * Schema attribute entity collection repository execution configuration array format:
     * @see ToolBoxSchemaAttrRepository::loadProfileSchema() configuration array format.
     *
     * @param integer $intAreaId
     * @param boolean $boolProfileSchemaGetRequired = false
     * @param boolean $boolProfileSchemaUpdateRequired = false
     * @param null|array $tabSchemaAttrEntityCollectionRepoExecConfig = null
     * @return boolean
     */
    public function loadProfileSchema(
        $intAreaId,
        $boolProfileSchemaGetRequired = false,
        $boolProfileSchemaUpdateRequired = false,
        array $tabSchemaAttrEntityCollectionRepoExecConfig = null
    )
    {
        // Reset provider
        $this->reset();

        // Return result
        return ToolBoxSchemaAttrRepository::loadProfileSchema(
            $this->objSchemaAttrProvider->getObjAttributeCollection(),
            $this->objSchemaAttrEntityCollectionRepo,
            $intAreaId,
            $boolProfileSchemaGetRequired,
            $boolProfileSchemaUpdateRequired,
            $tabSchemaAttrEntityCollectionRepoExecConfig
        );
    }



}