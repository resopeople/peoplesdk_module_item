<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\schema\attribute\model\SchemaAttrEntityCollection;
use people_sdk\item\schema\attribute\model\SchemaAttrEntityFactory;
use people_sdk\item\schema\attribute\model\repository\SchemaAttrEntitySimpleRepository;
use people_sdk\item\schema\attribute\model\repository\SchemaAttrEntitySimpleCollectionRepository;
use people_sdk\item\schema\attribute\model\repository\SchemaAttrEntityMultiRepository;
use people_sdk\item\schema\attribute\model\repository\SchemaAttrEntityMultiCollectionRepository;
use people_sdk\item\schema\attribute\provider\model\SchemaAttrProvider;
use people_sdk\module_item\schema_attribute\helper\SchemaAttrProviderHelper;



return array(
    // Schema attribute entity services
    // ******************************************************************************

    'people_item_schema_attr_entity_collection' => [
        'source' => SchemaAttrEntityCollection::class
    ],

    'people_item_schema_attr_entity_factory_collection' => [
        'source' => SchemaAttrEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_schema_attr_entity_factory' => [
        'source' => SchemaAttrEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_entity_factory_collection'],
            ['type' => 'class', 'value' => SchemaEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/schema_attribute/factory/schema_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_schema_attr_entity_simple_repository' => [
        'source' => SchemaAttrEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_schema_attr_entity_simple_collection_repository' => [
        'source' => SchemaAttrEntitySimpleCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_entity_simple_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_schema_attr_entity_multi_repository' => [
        'source' => SchemaAttrEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_schema_attr_entity_multi_collection_repository' => [
        'source' => SchemaAttrEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Schema attribute provider services
    // ******************************************************************************

    'people_item_schema_attr_entity_provider_collection' => [
        'source' => SchemaAttrEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_schema_attr_provider' => [
        'source' => SchemaAttrProvider::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_entity_provider_collection'],
            ['type' => 'config', 'value' => 'people/item/schema_attribute/provider/config'],
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_provider_cache_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Schema attribute helper services
    // ******************************************************************************

    'people_item_schema_attr_provider_helper' => [
        'source' => SchemaAttrProviderHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_provider'],
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_entity_simple_collection_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Preferences
    // ******************************************************************************

    SchemaAttrEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_schema_attr_entity_collection']
    ]
);