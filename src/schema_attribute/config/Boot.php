<?php

use people_sdk\module_item\schema_attribute\boot\SchemaAttrBootstrap;



return array(
    'people_item_schema_attr_bootstrap' => [
        'call' => [
            'class_path_pattern' => SchemaAttrBootstrap::class . ':boot'
        ]
    ]
);