<?php

use people_sdk\item\schema\attribute\model\SchemaAttrEntityFactory;
use people_sdk\item\schema\attribute\provider\model\SchemaAttrProvider;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'schema_attribute' => [
                // Schema attribute entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see SchemaAttrEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Schema entity factory execution configuration array format:
                     * @see SchemaAttrEntityFactory::setTabSchemaEntityFactoryExecConfig() configuration format.
                     */
                    'schema_factory_execution_config' => []
                ],

                // Schema attribute provider
                'provider' => [
                    /**
                     * Configuration array format:
                     * @see SchemaAttrProvider configuration format.
                     */
                    'config' => []
                ]
            ]
        ]
    ]
);