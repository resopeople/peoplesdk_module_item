<?php
/**
 * Description :
 * This class allows to define schema attribute module bootstrap class.
 * Schema attribute module bootstrap allows to boot schema attribute module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\schema_attribute\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\item\schema\attribute\model\SchemaAttrEntityFactory;
use people_sdk\module_item\schema_attribute\library\ConstSchemaAttr;



class SchemaAttrBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Schema attribute entity factory instance.
     * @var SchemaAttrEntityFactory
     */
    protected $objSchemaAttrEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param SchemaAttrEntityFactory $objSchemaAttrEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        SchemaAttrEntityFactory $objSchemaAttrEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objSchemaAttrEntityFactory = $objSchemaAttrEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstSchemaAttr::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set schema attribute entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'item', 'schema_attribute', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objSchemaAttrEntityFactory->getTabConfig(), $tabConfig);
            $this->objSchemaAttrEntityFactory->setTabConfig($tabConfig);
        };
    }



}