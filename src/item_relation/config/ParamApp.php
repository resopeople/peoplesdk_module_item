<?php

use people_sdk\item\item\relation\model\ItemRelationEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'item_relation' => [
                // Item relation entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see ItemRelationEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Schema entity relation factory execution configuration array format:
                     * @see ItemRelationEntityFactory::setTabSchemaRelationEntityFactoryExecConfig() configuration format.
                     */
                    'schema_relation_factory_execution_config' => [],

                    /**
                     * Item entity factory execution configuration array format:
                     * @see ItemRelationEntityFactory::setTabItemEntityFactoryExecConfig() configuration format.
                     */
                    'item_factory_execution_config' => []
                ]
            ]
        ]
    ]
);