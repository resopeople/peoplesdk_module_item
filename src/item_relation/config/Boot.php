<?php

use people_sdk\module_item\item_relation\boot\ItemRelationBootstrap;



return array(
    'people_item_item_relation_bootstrap' => [
        'call' => [
            'class_path_pattern' => ItemRelationBootstrap::class . ':boot'
        ]
    ]
);