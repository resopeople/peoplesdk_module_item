<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\schema\relation\model\SchemaRelationEntityFactory;
use people_sdk\item\item\model\ItemEntityFactory;
use people_sdk\item\item\relation\model\ItemRelationEntityCollection;
use people_sdk\item\item\relation\model\ItemRelationEntityFactory;
use people_sdk\item\item\relation\model\repository\ItemRelationEntityMultiRepository;
use people_sdk\item\item\relation\model\repository\ItemRelationEntityMultiCollectionRepository;



return array(
    // Item relation entity services
    // ******************************************************************************

    'people_item_item_relation_entity_collection' => [
        'source' => ItemRelationEntityCollection::class
    ],

    'people_item_item_relation_entity_factory_collection' => [
        'source' => ItemRelationEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_item_relation_entity_factory' => [
        'source' => ItemRelationEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_item_relation_entity_factory_collection'],
            ['type' => 'class', 'value' => SchemaRelationEntityFactory::class],
            ['type' => 'class', 'value' => ItemEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/item_relation/factory/schema_relation_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/item_relation/factory/item_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_item_relation_entity_multi_repository' => [
        'source' => ItemRelationEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_item_relation_entity_multi_collection_repository' => [
        'source' => ItemRelationEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_item_relation_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_item_relation_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    ItemRelationEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_item_relation_entity_collection']
    ]
);