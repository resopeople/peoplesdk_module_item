<?php
/**
 * Description :
 * This class allows to define item relation module bootstrap class.
 * Item relation module bootstrap allows to boot item relation module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\item_relation\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\item\item\relation\model\ItemRelationEntityFactory;
use people_sdk\module_item\item_relation\library\ConstItemRelation;



class ItemRelationBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Item relation entity factory instance.
     * @var ItemRelationEntityFactory
     */
    protected $objItemRelationEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param ItemRelationEntityFactory $objItemRelationEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        ItemRelationEntityFactory $objItemRelationEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objItemRelationEntityFactory = $objItemRelationEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstItemRelation::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set item relation entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'item', 'item_relation', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objItemRelationEntityFactory->getTabConfig(), $tabConfig);
            $this->objItemRelationEntityFactory->setTabConfig($tabConfig);
        };
    }



}