<?php

use people_sdk\item\area\schema\relation\scope\model\AreaRelationScopeEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'area_relation_scope' => [
                // Area relation scope entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see AreaRelationScopeEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Schema entity relation factory execution configuration array format:
                     * @see AreaRelationScopeEntityFactory::setTabSchemaRelationEntityFactoryExecConfig() configuration format.
                     */
                    'schema_relation_factory_execution_config' => [],

                    /**
                     * Area entity factory execution configuration array format:
                     * @see AreaRelationScopeEntityFactory::setTabAreaEntityFactoryExecConfig() configuration format.
                     */
                    'area_factory_execution_config' => []
                ]
            ]
        ]
    ]
);