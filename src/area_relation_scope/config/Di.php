<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\schema\relation\model\SchemaRelationEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\schema\relation\scope\model\AreaRelationScopeEntityCollection;
use people_sdk\item\area\schema\relation\scope\model\AreaRelationScopeEntityFactory;
use people_sdk\item\area\schema\relation\scope\model\repository\AreaRelationScopeEntityMultiRepository;
use people_sdk\item\area\schema\relation\scope\model\repository\AreaRelationScopeEntityMultiCollectionRepository;



return array(
    // Area relation scope entity services
    // ******************************************************************************

    'people_item_area_relation_scope_entity_collection' => [
        'source' => AreaRelationScopeEntityCollection::class
    ],

    'people_item_area_relation_scope_entity_factory_collection' => [
        'source' => AreaRelationScopeEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_relation_scope_entity_factory' => [
        'source' => AreaRelationScopeEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_area_relation_scope_entity_factory_collection'],
            ['type' => 'class', 'value' => SchemaRelationEntityFactory::class],
            ['type' => 'class', 'value' => AreaEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/area_relation_scope/factory/schema_relation_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/area_relation_scope/factory/area_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_relation_scope_entity_multi_repository' => [
        'source' => AreaRelationScopeEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_area_relation_scope_entity_multi_collection_repository' => [
        'source' => AreaRelationScopeEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_area_relation_scope_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_area_relation_scope_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    AreaRelationScopeEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_area_relation_scope_entity_collection']
    ]
);