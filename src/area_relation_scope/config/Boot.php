<?php

use people_sdk\module_item\area_relation_scope\boot\AreaRelationScopeBootstrap;



return array(
    'people_item_area_relation_scope_bootstrap' => [
        'call' => [
            'class_path_pattern' => AreaRelationScopeBootstrap::class . ':boot'
        ]
    ]
);