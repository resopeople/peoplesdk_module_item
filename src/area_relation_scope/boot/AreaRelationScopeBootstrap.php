<?php
/**
 * Description :
 * This class allows to define area relation scope module bootstrap class.
 * Area relation scope module bootstrap allows to boot area relation scope module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\area_relation_scope\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\item\area\schema\relation\scope\model\AreaRelationScopeEntityFactory;
use people_sdk\module_item\area_relation_scope\library\ConstAreaRelationScope;



class AreaRelationScopeBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Area relation scope entity factory instance.
     * @var AreaRelationScopeEntityFactory
     */
    protected $objAreaRelationScopeEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param AreaRelationScopeEntityFactory $objAreaRelationScopeEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        AreaRelationScopeEntityFactory $objAreaRelationScopeEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objAreaRelationScopeEntityFactory = $objAreaRelationScopeEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstAreaRelationScope::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set area relation scope entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'item', 'area_relation_scope', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objAreaRelationScopeEntityFactory->getTabConfig(), $tabConfig);
            $this->objAreaRelationScopeEntityFactory->setTabConfig($tabConfig);
        };
    }



}