<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\schema\attribute\provider\model\SchemaAttrProvider;
use people_sdk\item\item\attribute\value\model\ItemAttrValueEntityFactory;
use people_sdk\item\item\model\ItemEntityFactory;
use people_sdk\item\item\model\repository\ItemEntityMultiRepository;
use people_sdk\item\item\model\repository\ItemEntityMultiCollectionRepository;
use people_sdk\module_item\schema_attribute\helper\SchemaAttrProviderHelper;
use people_sdk\module_item\item\helper\ItemFactoryHelper;



return array(
    // Item attribute value entity services
    // ******************************************************************************

    'people_item_item_attr_value_entity_factory' => [
        'source' => ItemAttrValueEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'class', 'value' => SchemaAttrProvider::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Item entity services
    // ******************************************************************************

    'people_item_entity_factory' => [
        'source' => ItemEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'mixed', 'value' => null],
            ['type' => 'class', 'value' => UserProfileEntityFactory::class],
            ['type' => 'class', 'value' => AppProfileEntityFactory::class],
            ['type' => 'class', 'value' => SchemaEntityFactory::class],
            ['type' => 'dependency', 'value' => 'people_item_item_attr_value_entity_factory'],
            ['type' => 'config', 'value' => 'people/item/factory/user_profile_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/factory/app_profile_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/factory/schema_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/factory/item_attribute_value_factory_execution_config'],
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_entity_multi_repository' => [
        'source' => ItemEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_entity_multi_collection_repository' => [
        'source' => ItemEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Item helper services
    // ******************************************************************************

    'people_item_factory_helper' => [
        'source' => ItemFactoryHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_entity_factory'],
            ['type' => 'class', 'value' => SchemaAttrProviderHelper::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);