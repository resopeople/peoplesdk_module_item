<?php

use people_sdk\module_item\item\boot\ItemBootstrap;



return array(
    'people_item_bootstrap' => [
        'call' => [
            'class_path_pattern' => ItemBootstrap::class . ':boot'
        ]
    ]
);