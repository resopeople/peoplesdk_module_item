<?php

use people_sdk\item\item\model\ItemEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            // Item entity factory
            'factory' => [
                /**
                 * Configuration array format:
                 * @see ItemEntityFactory configuration format.
                 */
                'config' => [
                    'select_entity_require' => true,
                    'select_entity_create_require' => true,
                    'select_entity_collection_set_require' => true
                ],

                /**
                 * User profile entity factory execution configuration array format:
                 * @see ItemEntityFactory::setTabUserProfileEntityFactoryExecConfig() configuration format.
                 */
                'user_profile_factory_execution_config' => [],

                /**
                 * Application profile entity factory execution configuration array format:
                 * @see ItemEntityFactory::setTabAppProfileEntityFactoryExecConfig() configuration format.
                 */
                'app_profile_factory_execution_config' => [],

                /**
                 * Schema entity factory execution configuration array format:
                 * @see ItemEntityFactory::setTabSchemaEntityFactoryExecConfig() configuration format.
                 */
                'schema_factory_execution_config' => [],

                /**
                 * Item attribute value entity factory execution configuration array format:
                 * @see ItemEntityFactory::setTabItemAttrValueEntityFactoryExecConfig() configuration format.
                 */
                'item_attribute_value_factory_execution_config' => []
            ]
        ]
    ]
);