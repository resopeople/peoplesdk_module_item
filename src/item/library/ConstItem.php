<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\item\library;



class ConstItem
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'people_item_item';


    // Factory collection configuration
    const FACTORY_COLLECTION_KEY_RESET = 'reset';
    const FACTORY_COLLECTION_KEY_PATTERN_SCHEMA = 'schema_%1$s';
    const FACTORY_COLLECTION_KEY_PATTERN_PROFILE_SCHEMA = 'profile_schema_%1$s';



}