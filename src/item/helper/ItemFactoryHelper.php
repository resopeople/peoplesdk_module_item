<?php
/**
 * Description :
 * This class allows to define item factory helper class.
 * Item factory helper allows to provide features,
 * for item entity factory.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\item\helper;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\library\crypto\library\ToolBoxHash;
use people_sdk\item\item\model\ItemEntityCollection;
use people_sdk\item\item\model\ItemEntityFactory;
use people_sdk\module_item\schema_attribute\helper\SchemaAttrProviderHelper;
use people_sdk\module_item\item\library\ConstItem;



class ItemFactoryHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Item entity factory instance.
     * @var ItemEntityFactory
     */
    protected $objItemEntityFactory;



    /**
     * DI: Schema attribute provider helper instance.
     * @var SchemaAttrProviderHelper
     */
    protected $objSchemaAttrProviderHelper;



    /**
     * Associative array of item entity collection instances, used for schema.
     * @var ItemEntityCollection[]
     */
    protected $tabItemEntityCollection;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc

     * @param ItemEntityFactory $objItemEntityFactory
     * @param SchemaAttrProviderHelper $objSchemaAttrProviderHelper
     */
    public function __construct(
        ItemEntityFactory $objItemEntityFactory,
        SchemaAttrProviderHelper $objSchemaAttrProviderHelper
    )
    {
        // Init properties
        $this->objItemEntityFactory = $objItemEntityFactory;
        $this->objSchemaAttrProviderHelper = $objSchemaAttrProviderHelper;
        $this->tabItemEntityCollection = array();

        // Call parent constructor
        parent::__construct();
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Reset schema attributes, to get empty schema,
     * available for item entity factory.
     * Return true if success, false if an error occurs.
     *
     * @return boolean
     */
    public function reset()
    {
        // Init provider
        $result = $this->objSchemaAttrProviderHelper->reset();

        // Get item entity collection
        $strKey = ConstItem::FACTORY_COLLECTION_KEY_RESET;
        if(!array_key_exists($strKey, $this->tabItemEntityCollection))
        {
            $this->tabItemEntityCollection[$strKey] = new ItemEntityCollection();
        }
        $objItemEntityCollection = $this->tabItemEntityCollection[$strKey];

        // Init factory
        $this->objItemEntityFactory->setObjEntityCollection($objItemEntityCollection);

        // Return result
        return $result;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load schema attributes, to get schema,
     * available for item entity factory.
     * Return true if success, false if an error occurs.
     *
     * Schema attribute entity collection repository execution configuration array format:
     * @see SchemaAttrProviderHelper::loadSchema()
     * schema attribute entity collection repository execution configuration array format.
     *
     * @param integer $intSchemaId
     * @param null|array $tabSchemaAttrEntityCollectionRepoExecConfig = null
     * @return boolean
     */
    public function loadSchema(
        $intSchemaId,
        array $tabSchemaAttrEntityCollectionRepoExecConfig = null
    )
    {
        // Init provider
        $result = $this->objSchemaAttrProviderHelper->loadSchema(
            $intSchemaId,
            $tabSchemaAttrEntityCollectionRepoExecConfig
        );

        // Get item entity collection
        $strKey = sprintf(
            ConstItem::FACTORY_COLLECTION_KEY_PATTERN_SCHEMA,
            ToolBoxHash::getStrHash($intSchemaId)
        );
        if(!array_key_exists($strKey, $this->tabItemEntityCollection))
        {
            $this->tabItemEntityCollection[$strKey] = new ItemEntityCollection();
        }
        $objItemEntityCollection = $this->tabItemEntityCollection[$strKey];

        // Init factory
        $this->objItemEntityFactory->setObjEntityCollection($objItemEntityCollection);

        // Return result
        return $result;
    }



    /**
     * Load schema attributes, to get specified profile schemas,
     * available for item entity factory.
     * Return true if success, false if an error occurs.
     *
     * Schema attribute entity collection repository execution configuration array format:
     * @see SchemaAttrProviderHelper::loadProfileSchema()
     * schema attribute entity collection repository execution configuration array format.
     *
     * @param integer $intAreaId
     * @param boolean $boolProfileSchemaGetRequired = false
     * @param boolean $boolProfileSchemaUpdateRequired = false
     * @param null|array $tabSchemaAttrEntityCollectionRepoExecConfig = null
     * @return boolean
     */
    public function loadProfileSchema(
        $intAreaId,
        $boolProfileSchemaGetRequired = false,
        $boolProfileSchemaUpdateRequired = false,
        array $tabSchemaAttrEntityCollectionRepoExecConfig = null
    )
    {
        // Init provider
        $result = $this->objSchemaAttrProviderHelper->loadProfileSchema(
            $intAreaId,
            $boolProfileSchemaGetRequired,
            $boolProfileSchemaUpdateRequired,
            $tabSchemaAttrEntityCollectionRepoExecConfig
        );

        // Get item entity collection
        $strKey = sprintf(
            ConstItem::FACTORY_COLLECTION_KEY_PATTERN_PROFILE_SCHEMA,
            ToolBoxHash::getStrHash(array(
                $intAreaId,
                $boolProfileSchemaGetRequired,
                $boolProfileSchemaUpdateRequired
            ))
        );
        if(!array_key_exists($strKey, $this->tabItemEntityCollection))
        {
            $this->tabItemEntityCollection[$strKey] = new ItemEntityCollection();
        }
        $objItemEntityCollection = $this->tabItemEntityCollection[$strKey];

        // Init factory
        $this->objItemEntityFactory->setObjEntityCollection($objItemEntityCollection);

        // Return result
        return $result;
    }



}