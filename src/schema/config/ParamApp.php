<?php

use people_sdk\item\schema\model\SchemaEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'schema' => [
                // Schema entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see SchemaEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ]
                ]
            ]
        ]
    ]
);