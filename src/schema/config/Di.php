<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\schema\model\SchemaEntityCollection;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\schema\model\repository\SchemaEntityMultiRepository;
use people_sdk\item\schema\model\repository\SchemaEntityMultiCollectionRepository;



return array(
    // Schema entity services
    // ******************************************************************************

    'people_item_schema_entity_collection' => [
        'source' => SchemaEntityCollection::class
    ],

    'people_item_schema_entity_factory_collection' => [
        'source' => SchemaEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_schema_entity_factory' => [
        'source' => SchemaEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_schema_entity_factory_collection']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_schema_entity_multi_repository' => [
        'source' => SchemaEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_schema_entity_multi_collection_repository' => [
        'source' => SchemaEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_schema_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_schema_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    SchemaEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_schema_entity_collection']
    ]
);