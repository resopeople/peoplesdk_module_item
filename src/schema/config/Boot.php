<?php

use people_sdk\module_item\schema\boot\SchemaBootstrap;



return array(
    'people_item_schema_bootstrap' => [
        'call' => [
            'class_path_pattern' => SchemaBootstrap::class . ':boot'
        ]
    ]
);