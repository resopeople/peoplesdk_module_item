<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\schema\attribute\model\SchemaAttrEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\schema\attribute\scope\model\AreaAttrScopeEntityCollection;
use people_sdk\item\area\schema\attribute\scope\model\AreaAttrScopeEntityFactory;
use people_sdk\item\area\schema\attribute\scope\model\repository\AreaAttrScopeEntityMultiRepository;
use people_sdk\item\area\schema\attribute\scope\model\repository\AreaAttrScopeEntityMultiCollectionRepository;



return array(
    // Area attribute scope entity services
    // ******************************************************************************

    'people_item_area_attr_scope_entity_collection' => [
        'source' => AreaAttrScopeEntityCollection::class
    ],

    'people_item_area_attr_scope_entity_factory_collection' => [
        'source' => AreaAttrScopeEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_attr_scope_entity_factory' => [
        'source' => AreaAttrScopeEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_area_attr_scope_entity_factory_collection'],
            ['type' => 'class', 'value' => SchemaAttrEntityFactory::class],
            ['type' => 'class', 'value' => AreaEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/area_attribute_scope/factory/schema_attribute_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/area_attribute_scope/factory/area_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_attr_scope_entity_multi_repository' => [
        'source' => AreaAttrScopeEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_area_attr_scope_entity_multi_collection_repository' => [
        'source' => AreaAttrScopeEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_area_attr_scope_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_area_attr_scope_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    AreaAttrScopeEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_area_attr_scope_entity_collection']
    ]
);