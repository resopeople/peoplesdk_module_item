<?php

use people_sdk\item\area\schema\attribute\scope\model\AreaAttrScopeEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'area_attribute_scope' => [
                // Area attribute scope entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see AreaAttrScopeEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Schema attribute entity factory execution configuration array format:
                     * @see AreaAttrScopeEntityFactory::setTabSchemaAttrEntityFactoryExecConfig() configuration format.
                     */
                    'schema_attribute_factory_execution_config' => [],

                    /**
                     * Area entity factory execution configuration array format:
                     * @see AreaAttrScopeEntityFactory::setTabAreaEntityFactoryExecConfig() configuration format.
                     */
                    'area_factory_execution_config' => []
                ]
            ]
        ]
    ]
);