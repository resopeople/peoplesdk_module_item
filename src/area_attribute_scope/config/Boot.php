<?php

use people_sdk\module_item\area_attribute_scope\boot\AreaAttrScopeBootstrap;



return array(
    'people_item_area_attribute_scope_bootstrap' => [
        'call' => [
            'class_path_pattern' => AreaAttrScopeBootstrap::class . ':boot'
        ]
    ]
);