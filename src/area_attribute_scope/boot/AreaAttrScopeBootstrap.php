<?php
/**
 * Description :
 * This class allows to define area attribute scope module bootstrap class.
 * Area attribute scope module bootstrap allows to boot area attribute scope module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\area_attribute_scope\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\item\area\schema\attribute\scope\model\AreaAttrScopeEntityFactory;
use people_sdk\module_item\area_attribute_scope\library\ConstAreaAttrScope;



class AreaAttrScopeBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Area attribute scope entity factory instance.
     * @var AreaAttrScopeEntityFactory
     */
    protected $objAreaAttrScopeEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param AreaAttrScopeEntityFactory $objAreaAttrScopeEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        AreaAttrScopeEntityFactory $objAreaAttrScopeEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objAreaAttrScopeEntityFactory = $objAreaAttrScopeEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstAreaAttrScope::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set area attribute scope entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'item', 'area_attribute_scope', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objAreaAttrScopeEntityFactory->getTabConfig(), $tabConfig);
            $this->objAreaAttrScopeEntityFactory->setTabConfig($tabConfig);
        };
    }



}