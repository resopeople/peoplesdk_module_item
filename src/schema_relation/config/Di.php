<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\schema\relation\model\SchemaRelationEntityCollection;
use people_sdk\item\schema\relation\model\SchemaRelationEntityFactory;
use people_sdk\item\schema\relation\model\repository\SchemaRelationEntityMultiRepository;
use people_sdk\item\schema\relation\model\repository\SchemaRelationEntityMultiCollectionRepository;



return array(
    // Schema relation entity services
    // ******************************************************************************

    'people_item_schema_relation_entity_collection' => [
        'source' => SchemaRelationEntityCollection::class
    ],

    'people_item_schema_relation_entity_factory_collection' => [
        'source' => SchemaRelationEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_schema_relation_entity_factory' => [
        'source' => SchemaRelationEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_schema_relation_entity_factory_collection'],
            ['type' => 'class', 'value' => SchemaEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/schema_relation/factory/schema_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_schema_relation_entity_multi_repository' => [
        'source' => SchemaRelationEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_schema_relation_entity_multi_collection_repository' => [
        'source' => SchemaRelationEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_schema_relation_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_schema_relation_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    SchemaRelationEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_schema_relation_entity_collection']
    ]
);