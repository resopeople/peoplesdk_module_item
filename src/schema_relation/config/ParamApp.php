<?php

use people_sdk\item\schema\relation\model\SchemaRelationEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'schema_relation' => [
                // Schema relation entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see SchemaRelationEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Schema entity factory execution configuration array format:
                     * @see SchemaRelationEntityFactory::setTabSchemaEntityFactoryExecConfig() configuration format.
                     */
                    'schema_factory_execution_config' => []
                ]
            ]
        ]
    ]
);