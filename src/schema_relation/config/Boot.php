<?php

use people_sdk\module_item\schema_relation\boot\SchemaRelationBootstrap;



return array(
    'people_item_schema_relation_bootstrap' => [
        'call' => [
            'class_path_pattern' => SchemaRelationBootstrap::class . ':boot'
        ]
    ]
);