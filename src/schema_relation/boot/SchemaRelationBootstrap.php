<?php
/**
 * Description :
 * This class allows to define schema relation module bootstrap class.
 * Schema relation module bootstrap allows to boot schema relation module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\schema_relation\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\item\schema\relation\model\SchemaRelationEntityFactory;
use people_sdk\module_item\schema_relation\library\ConstSchemaRelation;



class SchemaRelationBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Schema relation entity factory instance.
     * @var SchemaRelationEntityFactory
     */
    protected $objSchemaRelationEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param SchemaRelationEntityFactory $objSchemaRelationEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        SchemaRelationEntityFactory $objSchemaRelationEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objSchemaRelationEntityFactory = $objSchemaRelationEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstSchemaRelation::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set schema relation entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'item', 'schema_relation', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objSchemaRelationEntityFactory->getTabConfig(), $tabConfig);
            $this->objSchemaRelationEntityFactory->setTabConfig($tabConfig);
        };
    }



}