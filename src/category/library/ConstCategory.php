<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\category\library;



class ConstCategory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'people_item_category';



}