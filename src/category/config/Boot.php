<?php

use people_sdk\module_item\category\boot\CategoryBootstrap;



return array(
    'people_item_category_bootstrap' => [
        'call' => [
            'class_path_pattern' => CategoryBootstrap::class . ':boot'
        ]
    ]
);