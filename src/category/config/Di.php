<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\category\model\CategoryEntityCollection;
use people_sdk\item\category\model\CategoryEntityFactory;
use people_sdk\item\category\model\repository\CategoryEntityMultiRepository;
use people_sdk\item\category\model\repository\CategoryEntityMultiCollectionRepository;



return array(
    // Category entity services
    // ******************************************************************************

    'people_item_category_entity_collection' => [
        'source' => CategoryEntityCollection::class
    ],

    'people_item_category_entity_factory_collection' => [
        'source' => CategoryEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_category_entity_factory' => [
        'source' => CategoryEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_category_entity_factory_collection']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_category_entity_multi_repository' => [
        'source' => CategoryEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_category_entity_multi_collection_repository' => [
        'source' => CategoryEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_category_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_category_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    CategoryEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_category_entity_collection']
    ]
);