<?php

use people_sdk\item\category\model\CategoryEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'category' => [
                // Category entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see CategoryEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ]
                ]
            ]
        ]
    ]
);