<?php
/**
 * Description :
 * This class allows to define area category scope module bootstrap class.
 * Area category scope module bootstrap allows to boot area category scope module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_item\area_category_scope\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\item\area\category\scope\model\AreaCategoryScopeEntityFactory;
use people_sdk\module_item\area_category_scope\library\ConstAreaCategoryScope;



class AreaCategoryScopeBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Area category scope entity factory instance.
     * @var AreaCategoryScopeEntityFactory
     */
    protected $objAreaCategoryScopeEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param AreaCategoryScopeEntityFactory $objAreaCategoryScopeEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        AreaCategoryScopeEntityFactory $objAreaCategoryScopeEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objAreaCategoryScopeEntityFactory = $objAreaCategoryScopeEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstAreaCategoryScope::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set area category scope entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'item', 'area_category_scope', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objAreaCategoryScopeEntityFactory->getTabConfig(), $tabConfig);
            $this->objAreaCategoryScopeEntityFactory->setTabConfig($tabConfig);
        };
    }



}