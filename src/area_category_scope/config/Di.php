<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\item\category\model\CategoryEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\category\scope\model\AreaCategoryScopeEntityCollection;
use people_sdk\item\area\category\scope\model\AreaCategoryScopeEntityFactory;
use people_sdk\item\area\category\scope\model\repository\AreaCategoryScopeEntityMultiRepository;
use people_sdk\item\area\category\scope\model\repository\AreaCategoryScopeEntityMultiCollectionRepository;



return array(
    // Area category scope entity services
    // ******************************************************************************

    'people_item_area_category_scope_entity_collection' => [
        'source' => AreaCategoryScopeEntityCollection::class
    ],

    'people_item_area_category_scope_entity_factory_collection' => [
        'source' => AreaCategoryScopeEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_category_scope_entity_factory' => [
        'source' => AreaCategoryScopeEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_item_area_category_scope_entity_factory_collection'],
            ['type' => 'class', 'value' => CategoryEntityFactory::class],
            ['type' => 'class', 'value' => AreaEntityFactory::class],
            ['type' => 'config', 'value' => 'people/item/area_category_scope/factory/category_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/item/area_category_scope/factory/area_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_area_category_scope_entity_multi_repository' => [
        'source' => AreaCategoryScopeEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_item_area_category_scope_entity_multi_collection_repository' => [
        'source' => AreaCategoryScopeEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_item_area_category_scope_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_item_area_category_scope_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    AreaCategoryScopeEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_item_area_category_scope_entity_collection']
    ]
);