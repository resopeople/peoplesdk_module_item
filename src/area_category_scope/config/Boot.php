<?php

use people_sdk\module_item\area_category_scope\boot\AreaCategoryScopeBootstrap;



return array(
    'people_item_area_category_scope_bootstrap' => [
        'call' => [
            'class_path_pattern' => AreaCategoryScopeBootstrap::class . ':boot'
        ]
    ]
);