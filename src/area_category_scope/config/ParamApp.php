<?php

use people_sdk\item\area\category\scope\model\AreaCategoryScopeEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            'area_category_scope' => [
                // Area category scope entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see AreaCategoryScopeEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Category entity factory execution configuration array format:
                     * @see AreaCategoryScopeEntityFactory::setTabCategoryEntityFactoryExecConfig() configuration format.
                     */
                    'category_factory_execution_config' => [],

                    /**
                     * Area entity factory execution configuration array format:
                     * @see AreaCategoryScopeEntityFactory::setTabAreaEntityFactoryExecConfig() configuration format.
                     */
                    'area_factory_execution_config' => []
                ]
            ]
        ]
    ]
);