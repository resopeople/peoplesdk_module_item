<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\format\model\FormatRepository;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'item' => [
            // Item cache configuration
            'cache' => [
                'register' => [
                    /**
                     * Configuration array format:
                     * @see DefaultTableRegister configuration format.
                     */
                    'config' => []
                ],

                /**
                 * Configuration array format:
                 * @see FormatRepository configuration format.
                 */
                'config' => [
                    'key_pattern' => 'people-item-%s',
                    'key_regexp_select' => '#people\-item\-(.+)#'
                ]
            ],

            'schema_attribute' => [
                'provider' => [
                    // Schema attribute provider cache configuration
                    'cache' => [
                        /**
                         * Configuration array format:
                         * @see FormatRepository configuration format.
                         */
                        'config' => [
                            'key_pattern' => 'people-item-schema-attr-provider-%s',
                            'key_regexp_select' => '#people\-item\-schema\-attr\-provider\-(.+)#'
                        ]
                    ]
                ]
            ]
        ]
    ]
);