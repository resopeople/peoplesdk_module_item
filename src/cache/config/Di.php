<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\format\model\FormatRepository;



return array(
    // Item cache services
    // ******************************************************************************

    'people_item_cache_register' => [
        'source' => DefaultTableRegister::class,
        'argument' => [
            ['type' => 'config', 'value' => 'people/item/cache/register/config'],
            ['type' => 'mixed', 'value' => null]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'people/item/cache/config'],
            ['type' => 'dependency', 'value' => 'people_item_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Schema attribute provider cache services
    // ******************************************************************************

    'people_item_schema_attr_provider_cache_register' => [
        'source' => DefaultTableRegister::class,
        'set' => ['type' => 'dependency', 'value' => 'people_item_cache_register'],
        'option' => [
            'shared' => true
        ]
    ],

    'people_item_schema_attr_provider_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'people/item/schema_attribute/provider/cache/config'],
            ['type' => 'dependency', 'value' => 'people_item_schema_attr_provider_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);