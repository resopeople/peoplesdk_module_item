PeopleSdk_Module_Item
=====================



Version "1.0.0"
---------------

- Create repository

- Set schema

- Set schema attribute

- Set schema relation

- Set category

- Set area

- Set area attribute scope

- Set area relation scope

- Set area category scope

- Set area member

- Set area group member

- Set item

- Set item relation

- Set item category

- Set cache

---


