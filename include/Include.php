<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/schema/library/ConstSchema.php');
include($strRootPath . '/src/schema/boot/SchemaBootstrap.php');

include($strRootPath . '/src/schema_attribute/library/ConstSchemaAttr.php');
include($strRootPath . '/src/schema_attribute/helper/SchemaAttrProviderHelper.php');
include($strRootPath . '/src/schema_attribute/boot/SchemaAttrBootstrap.php');

include($strRootPath . '/src/schema_relation/library/ConstSchemaRelation.php');
include($strRootPath . '/src/schema_relation/boot/SchemaRelationBootstrap.php');

include($strRootPath . '/src/category/library/ConstCategory.php');
include($strRootPath . '/src/category/boot/CategoryBootstrap.php');

include($strRootPath . '/src/area/library/ConstArea.php');
include($strRootPath . '/src/area/boot/AreaBootstrap.php');

include($strRootPath . '/src/area_attribute_scope/library/ConstAreaAttrScope.php');
include($strRootPath . '/src/area_attribute_scope/boot/AreaAttrScopeBootstrap.php');

include($strRootPath . '/src/area_relation_scope/library/ConstAreaRelationScope.php');
include($strRootPath . '/src/area_relation_scope/boot/AreaRelationScopeBootstrap.php');

include($strRootPath . '/src/area_category_scope/library/ConstAreaCategoryScope.php');
include($strRootPath . '/src/area_category_scope/boot/AreaCategoryScopeBootstrap.php');

include($strRootPath . '/src/area_member/library/ConstAreaMember.php');
include($strRootPath . '/src/area_member/boot/AreaMemberBootstrap.php');

include($strRootPath . '/src/area_group_member/library/ConstAreaGrpMember.php');
include($strRootPath . '/src/area_group_member/boot/AreaGrpMemberBootstrap.php');

include($strRootPath . '/src/item/library/ConstItem.php');
include($strRootPath . '/src/item/helper/ItemFactoryHelper.php');
include($strRootPath . '/src/item/boot/ItemBootstrap.php');

include($strRootPath . '/src/item_relation/library/ConstItemRelation.php');
include($strRootPath . '/src/item_relation/boot/ItemRelationBootstrap.php');

include($strRootPath . '/src/item_category/library/ConstItemCategory.php');
include($strRootPath . '/src/item_category/boot/ItemCategoryBootstrap.php');

include($strRootPath . '/src/cache/library/ConstCache.php');